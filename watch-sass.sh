#!/bin/bash
SLEEP_TIME=10
FILENAMES=('estilos' 'styles')

# Current dir
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )"/../.. >/dev/null 2>&1 && pwd )"
DIR="$DIR/public/css/"

echo 'Watching SCSS...'

while true; do
	for FILE in "${FILENAMES[@]}"; do
		FILENAME=$DIR/$FILE

		if test -f $FILENAME.scss; then
			CSS_FILE_TIME=$(stat $FILENAME.min.css -c %Y)
			SCSS_FILE_TIME=$(stat $FILENAME.scss -c %Y)

			if [ $CSS_FILE_TIME -lt $SCSS_FILE_TIME ]; then
				echo "SCSS file $FILE.scss modified at" $(stat $FILENAME.scss -c %y)
				sassc $FILENAME.scss --style compressed $FILENAME.min.css
				echo "CSS file $FILE.min.css generated at" $(stat $FILENAME.min.css -c %y)
			fi
		else 
			echo "$FILENAME.scss does not exist"
		fi

		sleep $SLEEP_TIME
	done
done
